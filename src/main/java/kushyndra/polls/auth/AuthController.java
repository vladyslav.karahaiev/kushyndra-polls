package kushyndra.polls.auth;

import java.util.*;
import java.util.stream.Collectors;

import javax.validation.Valid;

import kushyndra.polls.dto.JwtResponse;
import kushyndra.polls.dto.LoginRequest;
import kushyndra.polls.dto.MessageResponse;
import kushyndra.polls.dto.SignupRequest;
import kushyndra.polls.model.Role;
import kushyndra.polls.model.RoleEnum;
import kushyndra.polls.model.User;
import kushyndra.polls.persistence.RoleRepository;
import kushyndra.polls.persistence.UsersRepository;
import kushyndra.polls.security.jwt.JwtUtils;
import kushyndra.polls.security.services.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
   @Autowired
   AuthenticationManager authenticationManager;

   @Autowired
   UsersRepository userRepository;

   @Autowired
   RoleRepository roleRepository;

   @Autowired
   PasswordEncoder encoder;

   @Autowired
   JwtUtils jwtUtils;

   @PostMapping("/signin")
   public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

      Authentication authentication = authenticationManager.authenticate(
          new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

      SecurityContextHolder.getContext().setAuthentication(authentication);
      String jwt = jwtUtils.generateJwtToken(authentication);

      UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
      List<String> roles = userDetails.getAuthorities().stream()
          .map(GrantedAuthority::getAuthority)
          .collect(Collectors.toList());

      return ResponseEntity.ok(new JwtResponse(jwt,
          userDetails.getId(),
          userDetails.getUsername(),
          userDetails.getEmail(),
          roles));
   }

   @PostMapping("/signup")
   public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
      if (userRepository.existsByUsername(signUpRequest.getUsername())) {
         return ResponseEntity
             .badRequest()
             .body(new MessageResponse("Error: Username is already taken!"));
      }

      if (userRepository.existsByEmail(signUpRequest.getEmail())) {
         return ResponseEntity
             .badRequest()
             .body(new MessageResponse("Error: Email is already in use!"));
      }

      // Create new user's account
      User user = new User(signUpRequest.getUsername(),
          signUpRequest.getEmail(),
          encoder.encode(signUpRequest.getPassword()));

      Set<String> strRoles = signUpRequest.getRole();
      Set<Role> roles = new HashSet<>();


      if (strRoles == null) {
         Role userRole = roleRepository.findByName(RoleEnum.USER)
             .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
         roles.add(userRole);
      } else {
         strRoles.forEach(role -> {
            for(RoleEnum roleEnum: RoleEnum.values()){
               Role roleObj = roleRepository.findByName(roleEnum).orElseThrow(() -> new RuntimeException("Cant find RoleEnum in roleRepository"));
               if(roleObj.getName().toString().equals(role)){
                  roles.add(roleObj);
               }
            }
         });
      }

      user.setRoles(roles);
      userRepository.save(user);

      return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
   }


}