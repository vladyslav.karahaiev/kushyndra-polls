package kushyndra.polls.util;

import kushyndra.polls.exception.UserNotFoundException;
import kushyndra.polls.model.User;
import kushyndra.polls.persistence.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

@Component
public class UserArgumentResolver implements HandlerMethodArgumentResolver {

   @Autowired
   private UsersRepository usersRepo;

   @Override
   public boolean supportsParameter(MethodParameter parameter) {
      return parameter.getParameterType().equals(User.class);
   }

   @Override
   public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {

      String rawUserId = webRequest.getHeader("userId");

      if (rawUserId == null) {
         throw new RuntimeException("Header userId is not set");
      }
      int userId = Integer.parseInt(rawUserId);

      return this.usersRepo.findById(userId).orElseThrow(UserNotFoundException::new);
   }
}
