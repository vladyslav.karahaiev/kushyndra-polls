package kushyndra.polls.persistence;

import kushyndra.polls.model.Poll;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PollsRepository extends JpaRepository<Poll, Integer> {
}
