package kushyndra.polls.persistence;

import kushyndra.polls.model.Role;
import kushyndra.polls.model.RoleEnum;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Integer> {
   Optional<Role> findByName(RoleEnum name);
}
