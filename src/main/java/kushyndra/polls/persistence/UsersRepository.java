package kushyndra.polls.persistence;

import kushyndra.polls.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsersRepository extends JpaRepository<User, Integer> {
   Optional<User> findByUsername(String user);

   boolean existsByEmail(String email);

   boolean existsByUsername(String username);
}
