package kushyndra.polls.persistence;

import kushyndra.polls.model.Vote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface VotesRepository extends JpaRepository<Vote, Integer> {

   @Transactional
   @Modifying
   @Query(
       value = "delete from vote where user_id = ?1 and " +
           "answer_id in (select id from poll_answer where poll_id = ?2)" + "",
       nativeQuery = true
   )
   void deleteByUserAnswers(int userId, int pollId);

   @Query(
       value = "select v.id, v.answer_id, v.user_id " +
           "from vote v inner join poll_answer p " +
           "on v.answer_id = p.id " +
           "where p.poll_id = ?1",
       nativeQuery = true
   )
   List<Vote> findByPoll(int pollId);

   @Query(
       value = "select count(v.id) > 0 " +
           "from vote v inner join poll_answer p " +
           "on v.answer_id = p.id " +
           "where v.user_id = ?1 and p.poll_id = ?2",
       nativeQuery = true
   )
   boolean userHasVoted(int userId, int pollId);
}
