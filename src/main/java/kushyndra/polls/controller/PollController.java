package kushyndra.polls.controller;

import kushyndra.polls.dto.CreatePollDto;
import kushyndra.polls.dto.PollStats;
import kushyndra.polls.exception.ExceptionDto;
import kushyndra.polls.exception.PollNotFoundException;
import kushyndra.polls.model.Poll;
import kushyndra.polls.model.User;
import kushyndra.polls.service.PollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController("PollController")
@RequestMapping("/polls")
public class PollController {

   @Autowired
   private PollService pollService;

   @GetMapping
   public List<Poll> getPolls(User user) {
      return this.pollService.getPolls(user);
   }

   @GetMapping("/all")
   @PreAuthorize("hasAuthority('ADMIN_POLLS')")
   public List<Poll> getAllPolls(){
      return this.pollService.getPolls();
   }

   @PostMapping
   public ResponseEntity<Poll> create(
       User user,
       @Valid @RequestBody CreatePollDto createPollDto
   ) {
      Poll createdPoll = this.pollService.create(createPollDto, user);

      return new ResponseEntity<>(createdPoll, HttpStatus.CREATED);
   }

   @PutMapping
   public ResponseEntity<Poll> edit(
       User user,
       @Valid @RequestBody CreatePollDto createPollDto
   ) {
      Poll edited = this.pollService.create(createPollDto, user);

      return new ResponseEntity<>(edited, HttpStatus.OK);
   }


   @GetMapping("/{id}/stats")
   public PollStats getPollStats(
       User user,
       @PathVariable("id") int pollId
   ) {
      return this.pollService.getStats(pollId);
   }

   @DeleteMapping("/{id}/delete")
   @PreAuthorize("hasAuthority('ADMIN_POLLS') || getPrincipal().getUsername() == getThis().getPollService().getPoll(#pollId).getAuthor().getUsername()")
   public ResponseEntity<Poll> deletePoll(
           User user,
           @PathVariable("id") int pollId
   ) {
      Poll deleted = this.pollService.deletePoll(pollId);
      return new ResponseEntity<>(deleted, HttpStatus.OK);
   }


   @ResponseStatus(HttpStatus.NOT_FOUND)
   @ExceptionHandler(PollNotFoundException.class)
   public ExceptionDto handlePollNotFound(
       PollNotFoundException ex) {
      return new ExceptionDto(ex.getMessage(), HttpStatus.NOT_FOUND);
   }

   @ResponseStatus(HttpStatus.BAD_REQUEST)
   @ExceptionHandler(MethodArgumentNotValidException.class)
   public Map<String, String> handleValidationExceptions(
       MethodArgumentNotValidException ex) {
      Map<String, String> errors = new HashMap<>();

      ex.getBindingResult().getAllErrors().forEach((error) -> {
         String fieldName = ((FieldError) error).getField();
         String errorMessage = error.getDefaultMessage();
         errors.put(fieldName, errorMessage);
      });

      return errors;
   }

   public PollService getPollService(){
      return this.pollService;
   }
}
