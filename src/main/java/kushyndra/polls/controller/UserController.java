package kushyndra.polls.controller;

import kushyndra.polls.model.User;
import kushyndra.polls.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

   // DI using a field
   @Autowired
   private UserService userService;

   @GetMapping
   @PreAuthorize("hasAuthority('ADMIN_USERS')")
   public List<User> getUsers() {
      return this.userService.getUsers();
   }



}