package kushyndra.polls.controller;

import kushyndra.polls.dto.VotesDto;
import kushyndra.polls.exception.ExceptionDto;
import kushyndra.polls.exception.RepeatedVoteException;
import kushyndra.polls.model.User;
import kushyndra.polls.model.Vote;
import kushyndra.polls.service.VotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/polls")
public class VotesController {
   @Autowired
   private VotesService votesService;

   @PostMapping("/{id}/votes")
   public List<Vote> vote(
       @Valid @RequestBody VotesDto votesDto,
       @PathVariable("id") int pollId,
       User user
   ) {
      var votes = votesDto.getVotes().stream().toList();
      return votesService.vote(votes, user, pollId);
   }

   @DeleteMapping("/{id}/votes")
   public void retractVote(
       @PathVariable("id") int pollId,
       User user
   ) {
      this.votesService.retractVote(user.getId(), pollId);
   }


   @ResponseStatus(HttpStatus.CONFLICT)
   @ExceptionHandler(RepeatedVoteException.class)
   public ExceptionDto handleRepeatedVote(
       RepeatedVoteException ex) {
      return new ExceptionDto(ex.getMessage(), HttpStatus.CONFLICT);
   }

   @ResponseStatus(HttpStatus.BAD_REQUEST)
   @ExceptionHandler(MethodArgumentNotValidException.class)
   public Map<String, String> handleValidationExceptions(
       MethodArgumentNotValidException ex) {
      Map<String, String> errors = new HashMap<>();
      ex.getBindingResult().getAllErrors().forEach((error) -> {
         String fieldName = ((FieldError) error).getField();
         String errorMessage = error.getDefaultMessage();
         errors.put(fieldName, errorMessage);
      });
      return errors;
   }
}
