package kushyndra.polls.controller;

import kushyndra.polls.dto.CreatePollDto;
import kushyndra.polls.security.services.UserDetailsImpl;
import kushyndra.polls.service.PollService;
import kushyndra.polls.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;

@RestController
@RequestMapping("/thymeleaf")
public class ThymeleafController {

    @Autowired
    private PollService pollService;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;


    // Login form
    @RequestMapping("/login")
    public ModelAndView login() {
        ModelAndView mav = new ModelAndView("login");
        mav.addObject("loginError", false);
        return mav;
    }

    // Login form with error
    @RequestMapping("/login-error")
    public ModelAndView loginError() {
        ModelAndView mav = new ModelAndView("login");
        mav.addObject("loginError", true);
        return mav;
    }

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ModelAndView exception(final Throwable throwable) {
        String errorMessage = (throwable != null ? throwable.getMessage() : "Unknown error");
        ModelAndView mav = new ModelAndView("error");
        mav.addObject("errorMessage", errorMessage);
        return mav;
    }

    @RequestMapping("/polls")
    public ModelAndView showPolls(final CreatePollDto createPollDto, final ModelMap model){
        model.addAttribute("allPolls", this.pollService.getPolls());
        createPollDto.setQuestion("");
        createPollDto.setAnonymous(false);
        createPollDto.setMultipleAnswers(false);
        ArrayList<String> answers = new ArrayList<>(1);
        answers.add("");
        createPollDto.setAnswers(answers);
        return new ModelAndView("pollslist", model);
    }

    @RequestMapping(value="/polls", params={"addAnswer"})
    public ModelAndView addAnswerPolls(final CreatePollDto createPollDto, final BindingResult result, final ModelMap model){
        if(createPollDto.getAnswers()==null){
            ArrayList<String> answers = new ArrayList<>(1);
            answers.add("");
            createPollDto.setAnswers(answers);
        }else{
            createPollDto.getAnswers().add(new String());
        }
        model.addAttribute("allPolls", this.pollService.getPolls());
        return new ModelAndView("pollslist", model);
    }

    @RequestMapping(value="/polls", method=RequestMethod.POST)
    public ModelAndView createPoll(@Valid final CreatePollDto createPollDto, final BindingResult result, final ModelMap model){
        if (result.hasErrors()) {
            return showPolls(createPollDto, model);
        }
        if(!SecurityContextHolder.getContext().getAuthentication().isAuthenticated()){
            return login();
        }
        UserDetailsImpl details = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        this.pollService.create(createPollDto, userService.getUsers().stream().filter(u -> u.getId() == details.getId()).findFirst().get());
        return showPolls(createPollDto, model);
    }

    @GetMapping("/{id}/info")
    public ModelAndView getPollInfo(Model model, @PathVariable("id") int pollId){
        ModelAndView mav = new ModelAndView("pollinfo");
        mav.addObject("pollstats", this.pollService.getStats(pollId));
        return mav;
    }

}
