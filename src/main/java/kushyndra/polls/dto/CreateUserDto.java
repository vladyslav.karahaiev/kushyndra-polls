package kushyndra.polls.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CreateUserDto {
   @NotBlank
   private String username;

   @NotBlank
   private String password;
}
