package kushyndra.polls.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreatePollDto {

   @NotBlank
   private String question;

   @NotNull
   private boolean multipleAnswers;

   @NotNull
   private boolean anonymous;

   @NotEmpty
   private List<String> answers;
}
