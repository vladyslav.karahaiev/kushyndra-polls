package kushyndra.polls;


import kushyndra.polls.util.UserArgumentResolver;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.thymeleaf.extras.springsecurity5.dialect.SpringSecurityDialect;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import java.util.List;

@EnableWebMvc
@Configuration
@ComponentScan("kushyndra")
public class WebConfig implements WebMvcConfigurer, ApplicationContextAware {

   private ApplicationContext applicationContext;

   @Autowired
   private UserArgumentResolver userArgumentResolver;

   @Override
   public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
      argumentResolvers.add(userArgumentResolver);
   }


   @Bean
   public ThymeleafViewResolver thymeleafViewResolver() {
      ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
      viewResolver.setTemplateEngine(templateEngine());
      return viewResolver;
   }

   @Bean
   public SpringResourceTemplateResolver thymeleafTemplateResolver() {
      SpringResourceTemplateResolver templateResolver
              = new SpringResourceTemplateResolver();
      templateResolver.setApplicationContext(this.applicationContext);
      templateResolver.setPrefix("classpath:/templates/");
      templateResolver.setSuffix(".html");
      templateResolver.setTemplateMode(TemplateMode.HTML);
      return templateResolver;
   }


   @Bean
   public SpringTemplateEngine templateEngine() {
      SpringTemplateEngine templateEngine = new SpringTemplateEngine();
      templateEngine.setTemplateResolver(thymeleafTemplateResolver());
      templateEngine.addDialect(new SpringSecurityDialect());
      return templateEngine;
   }

   /*
   @Bean
   public ClassLoaderTemplateResolver secondaryTemplateResolver() {
      ClassLoaderTemplateResolver secondaryTemplateResolver = new ClassLoaderTemplateResolver();
      secondaryTemplateResolver.setPrefix("webapp/WEB-INF/templates/");
      secondaryTemplateResolver.setSuffix(".html");
      secondaryTemplateResolver.setTemplateMode(TemplateMode.HTML);
      secondaryTemplateResolver.setCharacterEncoding("UTF-8");
      secondaryTemplateResolver.setOrder(1);
      secondaryTemplateResolver.setCheckExistence(true);
      return secondaryTemplateResolver;
   }

    */

   @Override
   public void addResourceHandlers(ResourceHandlerRegistry registry) {
      registry.addResourceHandler("/img/**").addResourceLocations("classpath:/static/img/");
   }



   /*
   @Bean
   public ResourceBundleMessageSource messageSource() {
      ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
      messageSource.setBasename("messages");
      messageSource.setDefaultEncoding("UTF-8");
      messageSource.setUseCodeAsDefaultMessage(true);
      return messageSource;}
*/
   @Override
   public void setApplicationContext(final ApplicationContext applicationContext)
           throws BeansException {
      this.applicationContext = applicationContext;
   }
}
