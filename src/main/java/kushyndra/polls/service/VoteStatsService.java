package kushyndra.polls.service;

import kushyndra.polls.dto.AnswerStats;
import kushyndra.polls.model.Poll;

import java.util.List;

public interface VoteStatsService {
   List<AnswerStats> getStats(Poll poll);
}
