package kushyndra.polls.service.impl;

import kushyndra.polls.dto.AnswerStats;
import kushyndra.polls.model.Poll;
import kushyndra.polls.model.Vote;
import kushyndra.polls.persistence.VotesRepository;
import kushyndra.polls.service.VoteStatsService;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Setter
public class VoteStatsServiceImpl implements VoteStatsService {

   @Autowired
   private VotesRepository votesRepo;

   @Override
   public List<AnswerStats> getStats(Poll poll) {
      List<Vote> votes = this.votesRepo.findByPoll(poll.getId());

      Map<Integer, Integer> stats = this.countVotesStats(votes);

      return poll.getAnswers().stream()
          .map(answer -> {
             int number = answer.getNumber();
             int votesCount = Optional
                 .ofNullable(stats.get(number))
                 .orElse(0);

             return AnswerStats.builder()
                 .number(answer.getNumber())
                 .text(answer.getText())
                 .votes(votesCount)
                 .build();
          }).toList();
   }

   private Map<Integer, Integer> countVotesStats(List<Vote> votes) {
      Map<Integer, Integer> stats = new HashMap<>();

      votes.forEach(v -> {
         int key = v.getPollAnswer().getNumber();
         if (stats.containsKey(key)) {
            stats.put(key, stats.get(key) + 1);
         } else {
            stats.put(key, 1);
         }
      });

      return stats;
   }
}
