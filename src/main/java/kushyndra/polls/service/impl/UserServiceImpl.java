package kushyndra.polls.service.impl;

import kushyndra.polls.dto.CreateUserDto;
import kushyndra.polls.model.User;
import kushyndra.polls.persistence.UsersRepository;
import kushyndra.polls.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

   @Autowired
   private UsersRepository repo;

   private final RestTemplate restTemplate = new RestTemplate();

   @Override
   public User create(CreateUserDto createUserDto) {
      User user = User.builder()
          .username(createUserDto.getUsername())
          .build();

      return this.repo.save(user);
   }

   @Override
   public User login(CreateUserDto createUserDto) {
      String url = "https://jsonplaceholder.typicode.com/users";

      ResponseEntity<List> response = restTemplate.getForEntity(url, List.class);
      List<User> users = response.getBody();

      System.out.println(users);

      return User.builder()
          .username(createUserDto.getUsername())
          .build();
   }

   @Override
   public List<User> getUsers() {
      return this.repo.findAll();
   }

   @Override
   public boolean exists(int id) {
      return this.repo.existsById(id);
   }
}
