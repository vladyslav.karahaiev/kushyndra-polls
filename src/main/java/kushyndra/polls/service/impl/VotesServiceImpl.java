package kushyndra.polls.service.impl;

import kushyndra.polls.exception.*;
import kushyndra.polls.mapper.VotesMapper;
import kushyndra.polls.model.User;
import kushyndra.polls.model.Vote;
import kushyndra.polls.persistence.PollsRepository;
import kushyndra.polls.persistence.VotesRepository;
import kushyndra.polls.service.UserService;
import kushyndra.polls.service.VotesService;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service()
@Setter
@ToString
public class VotesServiceImpl implements VotesService {

   @Autowired
   private VotesRepository votesRepo;

   @Autowired
   private PollsRepository pollsRepo;

   @Autowired
   private VotesMapper mapper;

   @Autowired
   private UserService userService;

   @Override
   public List<Vote> vote(List<Integer> chosenAnswers, User user, int pollId) {
      this.validateVote(chosenAnswers, user.getId(), pollId);

      try {
         List<Vote> votes = this.mapper.mapToVotes(chosenAnswers, user);

         this.votesRepo.saveAll(votes);

         return votes;
      } catch (DataIntegrityViolationException e) {
         var message = e.getMostSpecificCause().getMessage();

         if (message.contains("fk_vote_to_answer"))
            throw new AnswerNotFoundException();
         else throw e;
      }
   }

   private void validateVote(List<Integer> chosenAnswers, int userId, int pollId) {
      if (chosenAnswers.isEmpty()) throw new NoAnswersException();

      if (this.userHasVoted(userId, pollId)) throw new RepeatedVoteException();

      var poll = this.pollsRepo.findById(pollId).orElseThrow(PollNotFoundException::new);

      if (!poll.isMultipleAnswers() && chosenAnswers.size() > 1) {
         throw new NoMultipleAnswersException();
      }
   }

   @Override
   public void retractVote(int userId, int pollId) {
      this.validateRetractVote(userId, pollId);
      this.votesRepo.deleteByUserAnswers(userId, pollId);
   }

   private void validateRetractVote(int userId, int pollId) {
      if (!this.pollsRepo.existsById(pollId)) throw new PollNotFoundException();
      if (!this.userHasVoted(userId, pollId)) throw new RetractVoteException();
   }

   private boolean userHasVoted(int userId, int pollId) {
      return this.votesRepo.userHasVoted(userId, pollId);
   }
}
