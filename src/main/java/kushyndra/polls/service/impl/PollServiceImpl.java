package kushyndra.polls.service.impl;

import kushyndra.polls.dto.AnswerStats;
import kushyndra.polls.dto.CreatePollDto;
import kushyndra.polls.dto.PollStats;
import kushyndra.polls.exception.PollNotFoundException;
import kushyndra.polls.mapper.PollMapper;
import kushyndra.polls.model.Poll;
import kushyndra.polls.model.PollAnswer;
import kushyndra.polls.model.User;
import kushyndra.polls.persistence.PollAnswerRepository;
import kushyndra.polls.persistence.PollsRepository;
import kushyndra.polls.service.PollService;
import kushyndra.polls.service.UserService;
import kushyndra.polls.service.VoteStatsService;
import lombok.Data;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;

@Data
@ToString
@Service
public class PollServiceImpl implements PollService {

   @Autowired
   private PollsRepository pollsRepo;

   @Autowired
   private PollAnswerRepository answerRepo;

   @Autowired
   private PollMapper mapper;

   @Autowired
   private VoteStatsService voteStatsService;

   @Autowired
   private UserService userService;

   @Override
   public Poll create(CreatePollDto createPoll, User user) {

      Poll poll = mapper.mapToPoll(createPoll, user);

      Poll createdPoll = this.pollsRepo.save(poll);

      List<PollAnswer> answers = poll.getAnswers()
          .stream().peek(answer -> answer.setPoll(createdPoll)).toList();

      this.answerRepo.saveAll(answers);

      createdPoll.setAnswers(answers
          .stream()
          .peek(answer -> answer.setPoll(null)
          ).toList());

      return createdPoll;
   }

   @Override
   public Poll edit(CreatePollDto poll, User user) {
      return this.create(poll, user);
   }

   @Override
   public boolean exists(int id) {
      return this.pollsRepo.existsById(id);
   }

   @Override
   public List<Poll> getPolls() {
      return this.pollsRepo.findAll();
   }

   @Override
   public List<Poll> getPolls(User author) {
      Poll example = new Poll();
      example.setAuthor(author);
      return this.pollsRepo.findAll(Example.of(example));
   }


   @Override
   public PollStats getStats(int pollId) {

      Poll poll = this.pollsRepo.findById(pollId)
          .orElseThrow(PollNotFoundException::new);

      List<AnswerStats> stats = this.voteStatsService.getStats(poll);

      return PollStats.builder()
          .question(poll.getQuestion())
          .stats(stats)
          .build();
   }

   @Override
   public Poll getPoll(int id) {
      return this.pollsRepo.findById(id).orElseThrow(PollNotFoundException::new);
   }

   @Override
   public Poll deletePoll(int id) {
      Poll poll = getPoll(id);
      this.pollsRepo.delete(poll);
      return poll;
   }
}
