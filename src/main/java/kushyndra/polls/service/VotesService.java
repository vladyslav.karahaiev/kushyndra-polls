package kushyndra.polls.service;

import kushyndra.polls.model.User;
import kushyndra.polls.model.Vote;

import java.util.List;

public interface VotesService {
   List<Vote> vote(List<Integer> votes, User user, int pollId);

   void retractVote(int userId, int pollId);
}
