package kushyndra.polls.service;

import kushyndra.polls.dto.CreatePollDto;
import kushyndra.polls.dto.PollStats;
import kushyndra.polls.model.Poll;
import kushyndra.polls.model.User;

import java.util.List;

public interface PollService {


   Poll create(CreatePollDto createPoll, User user);

   Poll edit(CreatePollDto createPoll, User user);

   boolean exists(int id);

   List<Poll> getPolls();

    List<Poll> getPolls(User author);

    PollStats getStats(int pollId);

    Poll getPoll(int id);

    Poll deletePoll(int id);

}
