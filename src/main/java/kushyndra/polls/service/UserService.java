package kushyndra.polls.service;

import kushyndra.polls.dto.CreateUserDto;
import kushyndra.polls.model.User;

import java.util.List;

public interface UserService {
   User create(CreateUserDto createUserDto);

   User login(CreateUserDto createUserDto);

   List<User> getUsers();

   boolean exists(int id);
}
