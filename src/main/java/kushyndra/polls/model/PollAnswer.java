package kushyndra.polls.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class PollAnswer {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;

   @Column
   private int number;

   @Column
   private String text;

   @ManyToOne
   @JoinColumn(name = "pollId", nullable = false)
   @JsonIgnore
   private Poll poll;
}
