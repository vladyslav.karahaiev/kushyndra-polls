package kushyndra.polls.model;

public enum RoleEnum {
   USER,
   ADMIN_POLLS,
   ADMIN_USERS
}