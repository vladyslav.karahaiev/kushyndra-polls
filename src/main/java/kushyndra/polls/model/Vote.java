package kushyndra.polls.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Vote {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;

   @ManyToOne
   @JoinColumn(
       name = "userId",
       nullable = false,
       foreignKey = @ForeignKey(name = "fk_vote_to_user")
   )
   private User user;

   @ManyToOne
   @JoinColumn(
       name = "answerId",
       nullable = false,
       foreignKey = @ForeignKey(name = "fk_vote_to_answer")
   )
   private PollAnswer pollAnswer;
}
