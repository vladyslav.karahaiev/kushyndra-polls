package kushyndra.polls.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Poll {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;

   @Column
   private String question;

   @Column
   private boolean multipleAnswers;

   @Column
   private boolean anonymous;

   @Column
   private boolean closed;

   @OneToMany(mappedBy = "poll")
   private List<PollAnswer> answers;

   @ManyToOne
   @JoinColumn(name = "authorId", nullable = false)
   private User author;
}
