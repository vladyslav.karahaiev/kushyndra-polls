package kushyndra.polls.mapper;

import kushyndra.polls.dto.CreatePollDto;
import kushyndra.polls.model.Poll;
import kushyndra.polls.model.PollAnswer;
import kushyndra.polls.model.User;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.IntStream;

@Component
public class PollMapper {

   public Poll mapToPoll(CreatePollDto createPoll, User user) {
      List<PollAnswer> indexedAnswers = this.indexAnswers(createPoll.getAnswers());

      return Poll.builder()
          .author(user)
          .question(createPoll.getQuestion())
          .anonymous(createPoll.isAnonymous())
          .answers(indexedAnswers)
          .multipleAnswers(createPoll.isMultipleAnswers())
          .build();
   }

   public List<PollAnswer> indexAnswers(List<String> answers) {
      return IntStream
          .range(0, answers.size())
          .mapToObj(i -> PollAnswer.builder()
              .number(i)
              .text(answers.get(i))
              .build()
          ).toList();
   }
}
