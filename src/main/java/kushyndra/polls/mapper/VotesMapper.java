package kushyndra.polls.mapper;

import kushyndra.polls.dto.AnswerStats;
import kushyndra.polls.model.PollAnswer;
import kushyndra.polls.model.User;
import kushyndra.polls.model.Vote;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class VotesMapper {

   public List<Vote> mapToVotes(List<Integer> chosenAnswers, User user) {
      return chosenAnswers.stream()
          .map(chosenAnswer -> Vote.builder()
              .pollAnswer(PollAnswer.builder().id(chosenAnswer).build())
              .user(user)
              .build()
          ).toList();
   }

   public List<AnswerStats> mapToAnswerStats(List<Vote> votes) {
      return votes.stream()
          .map(v -> new AnswerStats())
          .toList();
   }
}
