package kushyndra.polls;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.Arrays;
import java.util.Map;

@SpringBootApplication
public class PollsApplication {

   @Autowired
   Environment env;

   public static void main(String[] args) {
      SpringApplication.run(PollsApplication.class, args);
   }

   @EventListener
   public void handleContextRefresh(ContextRefreshedEvent event) {
      System.out.println(Arrays.toString(env.getActiveProfiles()));
      
      ApplicationContext applicationContext = event.getApplicationContext();

      RequestMappingHandlerMapping requestMappingHandlerMapping = applicationContext
          .getBean("requestMappingHandlerMapping", RequestMappingHandlerMapping.class);

      Map<RequestMappingInfo, HandlerMethod> map = requestMappingHandlerMapping
          .getHandlerMethods();

      map.forEach((key, value) -> System.out.println(key + " " + value));
   }

}
