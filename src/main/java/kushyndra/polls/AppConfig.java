package kushyndra.polls;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("kushyndra")
public class AppConfig {
}
