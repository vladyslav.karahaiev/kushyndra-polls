package kushyndra.polls.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ExceptionDto {
   private int status;

   private String error;

   private String message;

   public ExceptionDto(String message, HttpStatus status) {
      this.message = message;
      this.status = status.value();
      this.error = status.getReasonPhrase();
   }
}
