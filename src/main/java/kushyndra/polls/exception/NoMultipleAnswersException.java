package kushyndra.polls.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class NoMultipleAnswersException extends RuntimeException {
   public NoMultipleAnswersException() {
      super("Cannot give multiple answers to this poll");
   }
}
