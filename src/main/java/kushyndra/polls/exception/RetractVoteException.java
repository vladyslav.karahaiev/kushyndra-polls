package kushyndra.polls.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class RetractVoteException extends RuntimeException {
   public RetractVoteException() {
      super("You haven't voted in this poll yet");
   }
}
