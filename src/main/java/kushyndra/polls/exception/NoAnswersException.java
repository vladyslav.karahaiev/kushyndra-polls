package kushyndra.polls.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NoAnswersException extends RuntimeException {
   public NoAnswersException() {
      super("You must choose at least one answer");
   }
}
