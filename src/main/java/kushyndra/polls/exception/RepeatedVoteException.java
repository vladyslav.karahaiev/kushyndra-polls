package kushyndra.polls.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class RepeatedVoteException extends RuntimeException {
   public RepeatedVoteException() {
      super("You must retract your vote before voting again");
   }
}
