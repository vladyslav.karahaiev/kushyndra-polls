package kushyndra.polls.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import kushyndra.polls.dto.CreatePollDto;
import kushyndra.polls.dto.PollStats;
import kushyndra.polls.mapper.PollMapper;
import kushyndra.polls.model.Poll;
import kushyndra.polls.model.User;
import kushyndra.polls.persistence.*;
import kushyndra.polls.service.impl.PollServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static org.mockito.Mockito.doReturn;

@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@AutoConfigureWebTestClient
@WebMvcTest
public class PollControllerTest {

    @Autowired
    private WebTestClient webTestClient;
    @MockBean
    private UsersRepository usersRepository;
    @MockBean
    private RoleRepository roleRepository;
    @MockBean
    private PollsRepository pollsRepository;
    @MockBean
    private PollAnswerRepository pollAnswerRepository;
    @MockBean
    private VotesRepository votesRepository;

    @SpyBean
    private PollServiceImpl pollService;

    @Autowired
    private PollMapper mapper;

    private static ObjectWriter ow;

    @BeforeAll
    public static void setupBeforeAll() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ow = mapper.writer().withDefaultPrettyPrinter();
    }

    @Test
    @WithMockUser
    public void getPollsUser() throws Exception {
        User user = new User("user", "test@email.com","testpassowrd");
        user.setId(1);
        doReturn(Optional.of(user)).when(usersRepository).findById(1);
        webTestClient.get().uri("/polls")
                .header("userId", "1")
                .exchange().expectStatus().isOk().expectBody(List.class);
    }

    @Test
    public void getPollsUnauthorized() throws Exception {
        webTestClient.get().uri("/polls")
                .header("userId", "1")
                .exchange().expectStatus().isUnauthorized();
    }

    @Test
    @WithMockUser
    public void getPollsNoUser() throws Exception{
        webTestClient.get().uri("/polls")
                .header("userId", "1")
                .exchange().expectStatus().isNotFound();
    }

    @Test
    @WithMockUser(authorities = {"ADMIN_POLLS"})
    public void getAllPollsAdmin() throws Exception {
        webTestClient.get().uri("/polls/all")
                .exchange().expectStatus().isOk().expectBody(List.class);
    }

    @Test
    @WithMockUser
    public void getAllPollsUser() throws Exception {
        webTestClient.get().uri("/polls/all")
                .exchange().expectStatus().isForbidden();
    }

    @Test
    @WithMockUser
    public void createPollCorrect() throws Exception {
        User user = new User("user", "test@email.com","testpassword");
        user.setId(1);
        doReturn(Optional.of(user)).when(usersRepository).findById(1);
        CreatePollDto createPollDto = new CreatePollDto("Question", false, false, Arrays.asList("Answer1", "Answer2"));
        doReturn(mapper.mapToPoll(createPollDto, user)).when(pollService).create(createPollDto, user);
        webTestClient.post().uri("/polls")
                .header("userId", "1").contentType(MediaType.APPLICATION_JSON)
                .bodyValue(ow.writeValueAsString(createPollDto))
                .exchange().expectStatus().isCreated().expectBody(Poll.class);
    }

    @Test
    @WithMockUser
    public void createPollIncorrectUser() throws Exception {
        CreatePollDto createPollDto = new CreatePollDto("Question", false, false, Arrays.asList("Answer1", "Answer2"));
        webTestClient.post().uri("/polls")
                .header("userId", "1").contentType(MediaType.APPLICATION_JSON)
                .bodyValue(ow.writeValueAsString(createPollDto))
                .exchange().expectStatus().isNotFound();
    }

    @Test
    @WithMockUser
    public void createPollIncorrectCreatePollDto() throws Exception {
        User user = new User("user", "test@email.com","testpassword");
        user.setId(1);
        doReturn(Optional.of(user)).when(usersRepository).findById(1);
        CreatePollDto createPollDto = new CreatePollDto();
        webTestClient.post().uri("/polls")
                .header("userId", "1").contentType(MediaType.APPLICATION_JSON)
                .bodyValue(ow.writeValueAsString(createPollDto))
                .exchange().expectStatus().isBadRequest();
    }

    @Test
    @WithMockUser
    public void editPollCorrect() throws Exception {
        User user = new User("user", "test@email.com","testpassword");
        user.setId(1);
        doReturn(Optional.of(user)).when(usersRepository).findById(1);
        CreatePollDto createPollDto = new CreatePollDto("Question", false, false, Arrays.asList("Answer1", "Answer2"));
        doReturn(mapper.mapToPoll(createPollDto, user)).when(pollService).create(createPollDto, user);
        webTestClient.put().uri("/polls")
                .header("userId", "1").contentType(MediaType.APPLICATION_JSON)
                .bodyValue(ow.writeValueAsString(createPollDto))
                .exchange().expectStatus().isOk().expectBody(Poll.class);
    }

    @Test
    @WithMockUser
    public void editPollIncorrectUser() throws Exception {
        CreatePollDto createPollDto = new CreatePollDto("Question", false, false, Arrays.asList("Answer1", "Answer2"));
        webTestClient.put().uri("/polls")
                .header("userId", "1").contentType(MediaType.APPLICATION_JSON)
                .bodyValue(ow.writeValueAsString(createPollDto))
                .exchange().expectStatus().isNotFound();
    }

    @Test
    @WithMockUser
    public void editPollIncorrectCreatePollDto() throws Exception {
        User user = new User("user", "test@email.com","testpassword");
        user.setId(1);
        doReturn(Optional.of(user)).when(usersRepository).findById(1);
        CreatePollDto createPollDto = new CreatePollDto();
        webTestClient.put().uri("/polls")
                .header("userId", "1").contentType(MediaType.APPLICATION_JSON)
                .bodyValue(ow.writeValueAsString(createPollDto))
                .exchange().expectStatus().isBadRequest();
    }

    @Test
    @WithMockUser
    public void getPollStatsCorrect() throws Exception {
        User user = new User("user", "test@email.com","testpassword");
        user.setId(1);
        doReturn(Optional.of(user)).when(usersRepository).findById(1);
        doReturn(new PollStats()).when(pollService).getStats(1);
        webTestClient.get().uri("/polls/1/stats")
                .header("userId", "1")
                .exchange().expectStatus().isOk().expectBody(PollStats.class);
    }

    @Test
    @WithMockUser
    public void getPollStatsIncorrectUser() throws Exception {
        webTestClient.get().uri("/polls/1/stats")
                .header("userId", "1")
                .exchange().expectStatus().isNotFound();
    }

    @Test
    @WithMockUser
    public void getPollStatsIncorrectPoll() throws Exception {
        User user = new User("user", "test@email.com","testpassword");
        user.setId(1);
        doReturn(Optional.of(user)).when(usersRepository).findById(1);
        webTestClient.get().uri("/polls/1/stats")
                .header("userId", "1")
                .exchange().expectStatus().isNotFound();
    }

    @Test
    @WithMockUser(authorities = {"ADMIN_POLLS"})
    public void createAndDeletePollAdminCorrect() throws Exception{
        User user = new User("user", "test@email.com","testpassword");
        user.setId(1);
        doReturn(Optional.of(user)).when(usersRepository).findById(1);
        CreatePollDto createPollDto = new CreatePollDto("Question", false, false, Arrays.asList("Answer1", "Answer2"));
        doReturn(mapper.mapToPoll(createPollDto, user)).when(pollService).create(createPollDto, user);
        AtomicReference<Poll> poll = new AtomicReference<>();
        webTestClient.post().uri("/polls")
                .header("userId", "1").contentType(MediaType.APPLICATION_JSON)
                .bodyValue(ow.writeValueAsString(createPollDto))
                .exchange().expectStatus().isCreated().expectBody(Poll.class).consumeWith(pollEntityExchangeResult -> poll.set(pollEntityExchangeResult.getResponseBody()));
        user.setId(2);
        doReturn(Optional.of(user)).when(usersRepository).findById(2);
        doReturn(poll.get()).when(pollService).deletePoll(poll.get().getId());
        webTestClient.delete().uri("/polls/"+poll.get().getId()+"/delete")
                .header("userId", "2").exchange().expectStatus().isOk().expectBody(Poll.class);
    }

    @Test
    @WithMockUser
    public void createAndDeletePollAuthorCorrect() throws Exception{
        User user = new User("user", "test@email.com","testpassword");
        user.setId(1);
        doReturn(Optional.of(user)).when(usersRepository).findById(1);
        CreatePollDto createPollDto = new CreatePollDto("Question", false, false, Arrays.asList("Answer1", "Answer2"));
        doReturn(mapper.mapToPoll(createPollDto, user)).when(pollService).create(createPollDto, user);
        AtomicReference<Poll> poll = new AtomicReference<>();
        webTestClient.post().uri("/polls")
                .header("userId", "1").contentType(MediaType.APPLICATION_JSON)
                .bodyValue(ow.writeValueAsString(createPollDto))
                .exchange().expectStatus().isCreated().expectBody(Poll.class).consumeWith(pollEntityExchangeResult -> poll.set(pollEntityExchangeResult.getResponseBody()));
        doReturn(Optional.of(user)).when(usersRepository).findById(1);
        doReturn(poll.get()).when(pollService).getPoll(poll.get().getId());
        doReturn(poll.get()).when(pollService).deletePoll(poll.get().getId());
        webTestClient.delete().uri("/polls/"+poll.get().getId()+"/delete")
                .header("userId", "1").exchange().expectStatus().isOk().expectBody(Poll.class);
    }

    @Test
    @WithMockUser
    public void createAndDeletePollIncorrect() throws Exception{
        User user = new User("user", "test@email.com","testpassword");
        user.setId(1);
        doReturn(Optional.of(user)).when(usersRepository).findById(1);
        CreatePollDto createPollDto = new CreatePollDto("Question", false, false, Arrays.asList("Answer1", "Answer2"));
        doReturn(mapper.mapToPoll(createPollDto, user)).when(pollService).create(createPollDto, user);
        AtomicReference<Poll> poll = new AtomicReference<>();
        webTestClient.post().uri("/polls")
                .header("userId", "1").contentType(MediaType.APPLICATION_JSON)
                .bodyValue(ow.writeValueAsString(createPollDto))
                .exchange().expectStatus().isCreated().expectBody(Poll.class).consumeWith(pollEntityExchangeResult -> poll.set(pollEntityExchangeResult.getResponseBody()));
        user.setId(2);
        doReturn(poll.get()).when(pollService).getPoll(poll.get().getId());
        doReturn(Optional.of(user)).when(usersRepository).findById(2);
        doReturn(poll.get()).when(pollService).deletePoll(poll.get().getId());
        webTestClient.delete().uri("/polls/"+poll.get().getId()+"/delete")
                .header("userId", "2").exchange().expectStatus().isOk().expectBody(Poll.class);
    }
}
