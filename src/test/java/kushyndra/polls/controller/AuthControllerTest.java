package kushyndra.polls.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import kushyndra.polls.dto.LoginRequest;
import kushyndra.polls.dto.SignupRequest;
import kushyndra.polls.model.Role;
import kushyndra.polls.model.RoleEnum;
import kushyndra.polls.model.User;
import kushyndra.polls.persistence.UsersRepository;
import kushyndra.polls.security.services.UserDetailsImpl;
import kushyndra.polls.security.services.UserDetailsServiceImpl;
import kushyndra.polls.service.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.*;

import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AuthControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UsersRepository usersRepository;

    @MockBean
    private AuthenticationManager authenticationManager;

    @Autowired
    @InjectMocks
    private UserDetailsServiceImpl userDetailsService;
    @Autowired
    @InjectMocks
    private UserServiceImpl userService;

    private static ObjectWriter ow;

    @BeforeAll
    public static void setupBeforeAll(){
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ow = mapper.writer().withDefaultPrettyPrinter();
    }


    @BeforeEach
    public void setup(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }



    @Test
    @Order(1)
    public void registerNewUser() throws Exception {
        Set<String> roleSet = new HashSet<>();
        roleSet.add("user");
        SignupRequest request = new SignupRequest();
        request.setUsername("testUser");
        request.setEmail("test@email.com");
        request.setRole(roleSet);
        request.setPassword("testpassword");
        String requestJson = this.ow.writeValueAsString(request);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/api/auth/signup")
                .contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isOk());
    }

    @Test
    @Order(2)
    public void registerExistingUserByName() throws Exception{
        doReturn(true).when(usersRepository).existsByUsername("testUser");
        Set<String> roleSet = new HashSet<>();
        roleSet.add("user");
        SignupRequest request = new SignupRequest();
        request.setUsername("testUser");
        request.setEmail("test2@email.com");
        request.setRole(roleSet);
        request.setPassword("testpassword");
        String requestJson = this.ow.writeValueAsString(request);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/api/auth/signup")
                .contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Order(2)
    public void registerExistingUserByEmail() throws Exception{
        doReturn(true).when(usersRepository).existsByEmail("test@email.com");
        Set<String> roleSet = new HashSet<>();
        roleSet.add("user");
        SignupRequest request = new SignupRequest();
        request.setUsername("testUser2");
        request.setEmail("test@email.com");
        request.setRole(roleSet);
        request.setPassword("testpassword");
        String requestJson = this.ow.writeValueAsString(request);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/api/auth/signup")
                .contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isBadRequest());
    }


    @Test
    @Order(1)
    public void registerNewAdmin() throws Exception{
        Set<String> roleSet = new HashSet<>();
        roleSet.add("admin");
        SignupRequest request = new SignupRequest();
        request.setUsername("testAdmin");
        request.setEmail("test2@email.com");
        request.setRole(roleSet);
        request.setPassword("testpassword");
        String requestJson = this.ow.writeValueAsString(request);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/api/auth/signup")
                .contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isOk());
    }

    @Test
    @Order(2)
    public void loginExistingUser() throws Exception{
        LoginRequest request = new LoginRequest();
        request.setUsername("testUser");
        request.setPassword("testpassword");
        doReturn(new Authentication() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                return Collections.singletonList((GrantedAuthority) () -> "user");
            }

            @Override
            public Object getCredentials() {
                return request.getPassword();
            }

            @Override
            public Object getDetails() {
                User user = new User();
                //user.setId(1);
                user.setUsername(request.getUsername());
                user.setPassword(request.getPassword());
                user.setEmail("test@email.com");
                Set<Role> roleSet = new HashSet<>();
                Role userRole = new Role();
                userRole.setName(RoleEnum.USER);
                roleSet.add(userRole);
                user.setRoles(roleSet);
                return UserDetailsImpl.build(user);
            }

            @Override
            public Object getPrincipal() {
                return getDetails();
            }

            @Override
            public boolean isAuthenticated() {
                return true;
            }

            @Override
            public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

            }

            @Override
            public String getName() {
                return request.getUsername();
            }
        }).when(authenticationManager).authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));

        String requestJson = this.ow.writeValueAsString(request);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/api/auth/signin")
                .contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    @Order(2)
    public void loginNotExistingUser() throws Exception{
        LoginRequest request = new LoginRequest();
        request.setUsername("testNotExistingUser");
        request.setPassword("testpassword");
        String requestJson = this.ow.writeValueAsString(request);
        try {
            this.mockMvc.perform(MockMvcRequestBuilders.post("/api/auth/signin")
                    .contentType(MediaType.APPLICATION_JSON).content(requestJson));
        }catch (Exception ignored){
        }
    }

    @Test
    @Order(2)
    public void loginExistingUserBadPassword() throws Exception{
        LoginRequest request = new LoginRequest();
        request.setUsername("testUser");
        request.setPassword("badpassword");
        String requestJson = this.ow.writeValueAsString(request);
        try {
            this.mockMvc.perform(MockMvcRequestBuilders.post("/api/auth/signin")
                    .contentType(MediaType.APPLICATION_JSON).content(requestJson));
        }catch (Exception ignored){
        }
    }


}
