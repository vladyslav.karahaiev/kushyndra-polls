package kushyndra.polls.controller;

import kushyndra.polls.persistence.*;
import kushyndra.polls.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.doReturn;

@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@AutoConfigureWebTestClient
@WebMvcTest
public class UserControllerTest {
    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private UsersRepository usersRepository;
    @MockBean
    private RoleRepository roleRepository;
    @MockBean
    private PollsRepository pollsRepository;
    @MockBean
    private PollAnswerRepository pollAnswerRepository;
    @MockBean
    private VotesRepository votesRepository;
    @Autowired
    @InjectMocks
    private UserServiceImpl userService;

    @Test
    @WithMockUser
    public void getUsersUser(){
        webTestClient.get().uri("/users").exchange().expectStatus().isForbidden();
    }

    @Test
    public void getUsersUnauthorized(){
        webTestClient.get().uri("/users").exchange().expectStatus().isUnauthorized();
    }

    @Test
    @WithMockUser(authorities = {"ADMIN_USERS"})
    public void getUsersAdmin(){
        doReturn(new ArrayList<>()).when(usersRepository).findAll();
        webTestClient.get().uri("/users").exchange().expectStatus().isOk().expectBody(List.class);
    }
}
