package kushyndra.polls.service;

//
//import kushyndra.polls.exception.*;
//import kushyndra.polls.mapper.VotesMapper;
//import kushyndra.polls.model.Poll;
//import kushyndra.polls.model.Vote;
//import kushyndra.polls.fake_repo.IPollsRepository;
//import kushyndra.polls.fake_repo.IVotesRepository;
//import kushyndra.polls.fake_repo.error.ForeignKeyException;
//import kushyndra.polls.fake_repo.error.Table;
//import kushyndra.polls.service.impl.VotesServiceImpl;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mockito;
//
//import java.util.*;
//
//import static org.assertj.core.api.Assertions.*;
//import static org.mockito.Mockito.*;
//
public class VotesServiceTest {
//
//   private VotesServiceImpl service;
//   private IVotesRepository votesRepo;
//   private IPollsRepository pollsRepo;
//   private UserService userService;
//
//   private static final List<Integer> singleAnswerVote = Collections.singletonList(0);
//   private static final List<Integer> manyAnswersVote = Arrays.asList(0, 1);
//
//   @BeforeEach
//   public void setUp() {
//      votesRepo = Mockito.mock(IVotesRepository.class);
//      pollsRepo = Mockito.mock(IPollsRepository.class);
//      VotesMapper mapper = new VotesMapper();
//      userService = Mockito.mock(UserService.class);
//      service = new VotesServiceImpl(votesRepo, mapper, pollsRepo);
//      service.setUserService(userService);
//   }
//
//   @Test
//   public void vote_validInput_returnsMappedVotes() {
//      var userId = 1;
//      var pollId = 1;
//
//      var poll = new Poll();
//
//      when(userService.exists(userId)).thenReturn(true);
//      when(votesRepo.exists(any())).thenReturn(false);
//      when(pollsRepo.findOne(pollId)).thenReturn(Optional.of(poll));
//
//      var vote = Vote.builder()
//          .answerNumber(0)
//          .pollId(pollId)
//          .userId(userId)
//          .build();
//
//      assertThat(service.vote(singleAnswerVote, userId, pollId))
//          .isEqualTo(Collections.singletonList(vote));
//   }
//
//   @Test
//   public void vote_invalidUserId_throwsUserNotFound() {
//      var userId = 1;
//      var pollId = 1;
//
//      when(userService.exists(userId)).thenReturn(false);
//
//      assertThatExceptionOfType(UserNotFoundException.class)
//          .isThrownBy(() -> service.vote(singleAnswerVote, userId, pollId));
//   }
//
//   @Test
//   public void vote_emptyList_throwsNoAnswers() {
//      var userId = 1;
//      var pollId = 1;
//
//      when(userService.exists(userId)).thenReturn(true);
//
//      List<Integer> votes = new ArrayList<>();
//
//      assertThatExceptionOfType(NoAnswersException.class)
//          .isThrownBy(() -> service.vote(votes, userId, pollId));
//   }
//
//   @Test
//   public void vote_throwsRepeatedVote() {
//      var userId = 1;
//      var pollId = 1;
//
//      when(userService.exists(userId)).thenReturn(true);
//      when(votesRepo.exists(any())).thenReturn(true);
//
//      assertThatExceptionOfType(RepeatedVoteException.class)
//          .isThrownBy(() -> service.vote(singleAnswerVote, userId, pollId));
//   }
//
//   @Test
//   public void vote_invalidPollId_throwsPollNotFound() {
//      var userId = 1;
//      var pollId = 1;
//
//      when(userService.exists(userId)).thenReturn(true);
//      when(votesRepo.exists(any())).thenReturn(false);
//      when(pollsRepo.findOne(pollId)).thenReturn(Optional.empty());
//
//      assertThatExceptionOfType(PollNotFoundException.class)
//          .isThrownBy(() -> service.vote(singleAnswerVote, userId, pollId));
//   }
//
//   @Test
//   public void vote_manyAnswers_throwsNoMultipleAnswers() {
//      var userId = 1;
//      var pollId = 1;
//
//      var poll = Poll.builder()
//          .multipleAnswers(false)
//          .build();
//
//      when(userService.exists(userId)).thenReturn(true);
//      when(votesRepo.exists(any())).thenReturn(false);
//      when(pollsRepo.findOne(pollId)).thenReturn(Optional.of(poll));
//
//      assertThatExceptionOfType(NoMultipleAnswersException.class)
//          .isThrownBy(() -> service.vote(manyAnswersVote, userId, pollId));
//   }
//
//   @Test
//   public void vote_invalidAnswerNumber_throwsAnswerNotFound() {
//      var userId = 1;
//      var pollId = 1;
//
//      var poll = new Poll();
//
//      when(userService.exists(userId)).thenReturn(true);
//      when(votesRepo.exists(any())).thenReturn(false);
//      when(pollsRepo.findOne(pollId)).thenReturn(Optional.of(poll));
//      when(votesRepo.save(any())).thenThrow(new ForeignKeyException(Table.ANSWERS));
//
//      var inValidAnswer = Collections.singletonList(-1);
//
//      assertThatExceptionOfType(AnswerNotFoundException.class)
//          .isThrownBy(() -> service.vote(inValidAnswer, userId, pollId));
//   }
}
