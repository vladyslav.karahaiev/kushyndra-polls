# Kushyndra polls

Anonymous polls platform.

[Gitlab repository](https://gitlab.com/vladyslav.karahaiev/kushyndra-polls).

### **Start db:**

Dev:

```shell
docker run --name kp-postgres -e POSTGRES_USER=kp -e POSTGRES_PASSWORD=kp -e POSTGRES_DB=kp -p 5432:5432 -d --rm postgres:alpine
```

Prod:

```shell
docker run --name kp-postgres-prod -e POSTGRES_USER=kpprod -e POSTGRES_PASSWORD=kpprod -e POSTGRES_DB=kp -p 5431:5432 -d --rm postgres:alpine
```

